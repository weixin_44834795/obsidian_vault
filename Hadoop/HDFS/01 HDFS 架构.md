## 介绍

Hadoop 分布式文件系统 (HDFS) 是设计用来运行于商用硬件上的分布式文件系统. 与现存的分布式文件系统有许多相似之处. 然而, 它与其他的分布式系统有着显著的差异. HDFS 有着高容错性, 被设计部署于廉价硬件上. HDFS 为访问应用数据提了供高吞吐量, 并且适用于拥有大型数据集的应用. HDFS 放宽了部分 POSIX 需求, 使得能够使用流式访问文件系统数据. HDFS 起初构建作为 Apache Nutch 网络搜索引擎项目的基础架构. HDFS 是 Apache Hadoop Core 项目的一部分. 项目 URL 为 [http://hadoop.apache.org/](http://hadoop.apache.org/).

## 假设与目标

### 硬件故障

硬件故障是常态而非异常. 一个 HDFS 实例可能由上百或上千台服务器组成, 每台服务器存储着文件系统数据的一部分. 事实上, 存在着大量组件, 而且每个组件都有着非凡的故障概率, 这意味着 HDFS 总会存在一些组件处于非功能态. 因此, 故障检测和快速自动恢复是 HDFS 的一个核心架构目标.

### 流式数据访问

在 HDFS 上运行的应用需要对其数据集进行流式访问. 这些应用并非通常运行于通用文件系统的通用应用. HDFS 为批处理设计, 而非面向用户的交互式使用. 重点在于高吞吐量的数据访问, 而非低延迟的数据访问. POSIX 设定了许多硬性需求, 这对于以 HDFS 为目标的应用程序是不需要的. 为提高吞吐率, 对 POSIX 语义在部分关键领域进行了取舍.

### 大型数据集

在 HDFS 上运行的应用程序拥有大型数据集. 一个 HDFS 中的典型文件的尺寸在 GB 到 TB 级别. 因此, HDFS 针对大型文件进行优化. 它应当提供 high aggregate 数据带宽, 并且能扩展到同一集群的上百个节点上. 它应当在同一实例中支持数千万个文件.

### 简单一致性模型

HDFS 应用程序需要一个对文件单次写入多次读取的访问模型. 一个文件一旦被创建, 写入, 并关闭就不再改动, 除了追加或截断. 支持在文件末尾进行追加内容, 但不能在任意位置进行更新. 这个假设简化了数据一致性问题, 并且支持高吞吐量数据访问. MapReduce 应用或网络爬虫应用完全符合这类模型.

### “移动计算比移动数据更廉价”

一个应用程序的计算请求在其操作的数据附近执行将更加高效. 特别是在数据集很大是情况下. 这减少了网络阻塞, 并增加了系统总体的吞吐量. 这个假设通常将计算迁移到数据附近, 而非将数据移动到应用程序附近. HDFS 为应用程序提供了将自身移动到数据所在位置的接口.

### 跨异构软硬件平台的可移植性

HDFS 为便捷地从一个平台移植到另一个平台而设计. 这促使大量应用选 HDFS 择作为首选平台广泛采用.

## NameNode 和 DataNodes

HDFS 使用主/从架构. 一个 HDFS 集群包含一个 NameNode, 作为管理文件系统命名空间和控制客户端对文件的访问的主服务器. 此外, 还有大量的 DataNodes, 通常每个集群节点一个, 用于管理其节点附带的存储空间. HDFS 暴露了文件系统命名空间, 并且允许将用户数据存储在文件中. 在内部, 一个文件被分片存储在一个或多个块中, 这些块存储在一组 DataNodes 中. NameNode 执行文件系统命名空间操作, 像打开, 关闭和重命名文件与目录. 它也决定了块到 DataNodes 的映射. DataNodes 负责为来自文件系统的客户端的读写请求提供服务. DataNodes 也基于 NameNode 的指令执行块创建, 删除和复制.

![HDFS Architecture](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/images/hdfsarchitecture.png)

NameNode 和 DataNode 是设计用于运行在商用机器上的软件. 这些机器通常运行着 GNU/Linux 操作系统 (OS). HDFS 使用 Java 构建, 任何支持 Java 的机器都能运行 NameNode 或 DataNode. 使用高可移植性的 Java 意味着 HDFS 能够部署到很多机器上. 典型的部署是使用一台专用的机器仅运行 NameNode. 集群中其他的每台机器运行一个 DataNode 实例. 这种架构不排除在同一台机器上运行多个 DataNodes, 但在实际的部署中这种情况很少见.

集群中单个 NameNode 的存在极大地简化了系统架构. NameNode 是所有 HDFS 元数据的仲裁者和仓库. 该系统是以用户数据永远不会流经 NameNode 的方式设计的.

## 文件系统命名空间

HDFS 支持传统的分层文件组织结构. 一个用户或应用能够创建目录并向其中存储文件. 文件系统命名空间层级与大多数其他现存的问价系统类似; 它创建和移除文件, 从一个目录向另一个移动文件, 或重命名文件. HDFS 支持 [用户限额](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsDesign.htmlHdfsQuotaAdminGuide.html) 和 [访问权限](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsDesign.htmlHdfsPermissionsGuide.html). HDFS 不支持软/硬链接. 然而, HDFS 架构不妨碍实现这些功能.

尽管 HDFS 遵循 [文件系统命名约定](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsDesign.html../hadoop-common/filesystem/model.html#Paths_and_Path_Elements), 但部分路径和名称 (e.g. `/.reserved` and `.snapshot` ) 是把保留的. 像 [透明加密](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsDesign.htmlTransparentEncryption.html) 和 [快照](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsDesign.htmlHdfsSnapshots.html) 这些功能占用了保留路径.

NameNode 维护着文件系统命名空间. 任何对文件系统命名空间或其属性的改动都由 NameNode 记录. 应用程序能够指定 HDFS 应当维护的文件副本数. 文件的副本数被叫做文件的复制因子 (replication factor). 这些信息由 NameNode 存储.

## 数据复制

HDFS 被设计用于在跨多台设备的大型集群上可靠地存储非常巨大的文件. 它将每个文件存储为块序列. 文件块通过复制容错性. 每个文件的块大小和复制因子都是可配置的.

除了最后一块, 同一文件的所有块大小都相同, 因此用户可以在支持向可变长块追加和行同步 (hsync) 被加入后, 在未将最后一块填充至指定的块大小时开启一个新的块.

应用程序能够指定文件的副本数. 复制因子在文件创建时指定, 并可以在随后更改. HDFS 中的文件是一次性写入的 (除了追加和截断), 并且无论何时都只有一个写入者.

NameNode 对块副本执行所有决策. 它定期从集群中每个 DataNode 接收 Heartbeat 和 Blockreport. 接收到 Heartbeat 表明了 DataNode 运行正常. Blockreport 包含了一个 DataNode 中所有块的列表.

![HDFS DataNodes](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/images/hdfsdatanodes.png)

### 副本放置: 第一步

副本的放置对 HDFS 的可靠性和性能十分重要. 优化副本放置使 HDFS 区别于大多数其他的分布式文件系统. 这是一个需要大量调整和经验的功能. 机架感知 (rack-aware) 副本放置策略是为了提高数据可靠性, 可用性和网络带宽利用率. 当期副本放置策略的实现是朝着这个方向的首次努力. 实现该策略的短期目标是在生产系统上验证它, 对它的行为了解更深入, 并为测试和研究更多的复杂策略建立基础.

大型 HDFS 实例运行在一个计算机集群上, 通常其分布跨多个机架. 不同机架上两个节点需要通过交换机通信. 多数情况下, 同一机架的设备间网络带宽要高于不同机架的设备.

NameNode 通过 [Hadoop 机架感知](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/RackAwareness.html) 概述中的过程确定每个 DataNode 所属的机架 id. 一个简单但非最优的策略是将副本放置在不同的机架上. 这可以防止在整个机架故障时数据丢失, 并允许在读取数据时使用多个机架的带宽. 这个策略均匀地将副本分布在集群中, 这使组件故障时更容易负载均衡. 然而, 该策略增加了写入开销, 因为写入时需要将块传输至多个机架.

对于常见情况, 当复制因子是 3 时, HDFS 的分布策略是: 如果写入者在 datanode 上, 则将一个副本放置在本地设备, 否则放置在同一机架上随机的 datanode; 另一个副本放置在不同 (远程) 机架的节点中; 最后一个副本放置在同一远程机架的不同节点中. 这种策略削减了机架见的写入流量, 通常提高了写入性能. 机架故障的几率远比节点故障要低; 该策略不会影响数据可靠性和可用性保障. 然而, 它并没有降低读取数据占用的总带宽, 因为从一个块仅被放置在两台不同的机架, 而不是三台. 通过该策略, 块副本并没有均匀的跨机架分布. 两个副本位于同一机架不同节点上, 其余副本放置在其他机架的某个节点上. 该策略提高了写入性能, 没有影响到数据可靠性与读取性能.

当复制因子大于 3 时, 第 4 个以及其后的副本的位置将随机决定, 同时保持每台机架的副本数低于上限 (基本上是 `(replicas - 1) / racks + 2`).

因为 NameNode 不允许 DataNodes 拥有同一块的多个副本, 副本数的最大值是创建时 DataNodes 的总数.

在 HDFS 添加了对 [存储类型和存储策略](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/ArchivalStorage.html) 的支持后, 除了上述的机架感知外, NameNode 放置副本时还考虑了这些策略. NameNode 首先基于机架感知选择节点, 之后检查候选节点是否具有文件相关策略所需的存储类型. 如果候选节点没有存储类型,  NameNode 将寻找其他节点. 如果在第一条路径中无法找到足够的节点放置副本, NameNode 将在第二条路径中寻找拥有后背存储类型的节点.

目前, 上述的默认副本放置策略正在推进中.

### 副本选择

为减少全局带宽消耗和读取延迟, HDFS 试图从距读取者最近的副本满足读取需求. 如果存在与读取节点位于相同机架的副本, 则使用该副本满足读取需求. 若 HDFS 集群横跨多个数据中心, 则使用位于本地数据中心的副本, 而非任何远程副本.

### 块放置策略

如上所述, 当复制因子是 3 时, HDFS 的分布策略是: 如果写入者在 datanode 上, 则将一个副本放置在本地设备, 否则放置在同一机架上随机的 datanode; 另一个副本放置在不同 (远程) 机架的节点中; 最后一个副本放置在同一远程机架的不同节点中. 当复制因子大于 3 时, 第 4 个以及其后的副本的位置将随机决定, 同时保持每台机架的副本数低于上限 (基本上是 `(replicas - 1) / racks + 2`). 除此之外, HDFS 支持 4 种不同的可插拔 [块放置策略](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsBlockPlacementPolicies.html). 用户可以基于他们的底层结构和使用情况选择策略. 默认情况下 HDFS 提供 BlockPlacementPolicyDefault.

### 安全模式

在启动时, NameNode 将进入一个特殊的 Safemode 状态. NameNode 在 Safemode 状态下数据块不进行复制. NameNode 接收来自 DataNodes 的 Heartbeat 和 Blockreport 信息. 一个 Blockreport 包含了该 DataNode 所托管的数据块列表. 每个块都有指定的最小副本数. 当数据块的最小副本数被 NameNode 检入, 则认为该块已被安全复制. 当可配置的安全复制的数据块百分比由 NameNode 检入后 (加上额外的 30 秒), NameNode 将退出 Safemode 状态. 之后确定依旧少于指定副本数的数据块列表 (如果存在). 之后 NameNode 复制这些块到其他 DataNodes.

## 文件系统元数据持久性

HDFS 命名空间由 NameNode 存储. NameNode 使用被称为 EditLog 的事务日志持久化记录文件系统元数据的每项改动. 例如, 在 HDFS 中创建一个新文件, 将导致 NameNode 插入向 EditLog 插入一条新记录表明该操作. 同样, 更改文件的复制因子也将导致向 EditLog 中插入一条新纪录. NameNode 使用一份本地主机文件系统的文件存储 EditLog. 整个文件系统的 namespace, 包括块到文件的映射和文件系统属性, 存储在文件 FsImage 中. FsImage 也作为一个文件存储在 NameNode 的本地文件系统中.

NameNode 在内存中维护着整个文件系统命名空间和文件块映射的镜像. 当 NameNode 启动, 或由配置的阈值触发检查点时, 将从磁盘读入 FsImage 和 EditLog, 将 EditLog 中所有事务应用至 FsImage 在内存中的体现, 并将新的版本刷新到磁盘中新的 FsImage 上. 然后旧的 EditLog 可以被截断, 因为这些事务已经被应用到持久化的 FsImage 中. 这个过程被称为检查点. 检查点的目的是通过创建文件系统元数据快照, 并保存至 FsImage, 确保 HDFS 文件系统元数据具有一致的试图. 尽管读取 FsImage 很有效, 但直接对 FsImage 进行增量编辑却是低效的. 我们通过将编辑记录保存在 Editlog, 取代为每次编辑修改 FsImage. 通过检查点, 来自 Editlog 的改动将应用在 FsImage 上. 检查点可以由一个给定的, 以秒为单位的时间间隔触发 (`dfs.namenode.checkpoint.period`), 或是达到给定的文件系统事务累积量后触发 (`dfs.namenode.checkpoint.txns`). 如果同时设定了这两个属性, 当抵达第一个阈值后将触发一次检查点.

DataNode 在本地文件系统中以文件存储着 HDFS 数据. DataNode 并不知道 HDFS 文件. 它在本地文件系统在以单独的文件存储每个 HDFS 数据块. DataNode 没有将所有文件创建在同一个目录下. 相反, 它使用了一种启发式算法决定每个目录下最优的文件数量, 并且适当地创建子目录. 将所有本地文件创建在同一目录下并不是最优的方案, 因为本地文件系统可能并不能够有效地支持大量文件在同一目录下. 当一个 DataNode 启动, 它将扫描本地文件系统, 生成与每个本地文件相对应的所有 HDFS 数据块的清单, 并将其报告给 NameNode. 这个报告就是 _Blockreport_.

## 通信协议

所有 HDFS 通信协议都位于 TCP/IP 协议之上. 客户端向 NameNode 所在设备可配置的 TCP 端口建立连接. 客户端通过客户端协议与 NameNode 交流. DataNodes 使用 DataNode 协议与 NameNode 交流. 远程过程调用 (RPC) 抽象封装了客户端协议和 DataNode 协议. 根据设计, NameNode 绝不会发起任何 RPC. 相反, 它只响应 DataNodes 或客户端提出的 RPC 请求.

## 健壮性

HDFS 的主要目标是在即使存在故障的情况下可靠地存储数据. 最常见的三种故障类型是 NameNode 故障, DataNode 故障和网络分区故障.

### 数据磁盘故障, Heartbeats 和重新复制

每个 DataNode 定期向 NameNode 发送 Heartbeat 信息. 网络分区会导致一组 DataNodes 与 NameNode 失去连接. NameNode 通过 Heartbeat 信息缺失探测到该情况. NameNode 将近期没有 Heartbeats 的 DataNodes 标记死亡, 并且不再向其转发任何新的 IO 请求. 任何注册在死亡 DataNode 的数据对于 HDFS 不再可用. DataNode 死亡可能会导致部分块的复制因子低于所指定的值. NameNode 会持续跟踪需要复制的块, 并在必要的时候进行复制. 重新复制的必要性会由许多原因产生: DataNode 可能变得不可用, 副本可能损毁, DataNode 的硬盘可能发生故障, 或者文件的复制因子提高.

标记 DataNodes 死亡的超时时间相当长 (默认超过 10 分钟), 以避免由 DataNodes 属性波动导致的 "复制风暴". 用户能够设置更短的间隔标记 DataNodes 为陈旧, 并通过配置对性能敏感的工作负载避免陈读取或写入陈旧节点.

### 集群重新平衡

HDFS 架构与数据平衡方案相兼容. 当某个 DataNode 的空闲空间低于指定的阈值时, 数据平衡方案可能会自动地将数据由该 DataNode 转移到另一个. 当突然对某个特定文件提出高需求, 数据平衡方案可能动态地闯将额外副本, 并平衡集群中的其他数据. 这些数据平衡方案尚未实现.

### 数据完整性

从 DataNode 拉取的数据块可能损毁. 由于存储设备故障, 网络故障或软件漏洞, 可能导致损毁发生. HDFS 客户端软件对 HDFS 文件内容实行校验和检查. 当客户端创建了一个 HDFS 文件, 它会为文件的每个块计算校验和并存储在同一 HDFS 命名空间中一个单独的隐藏文件里. 当客户端取回文件内容时, 它将验证从每个 DataNode 收到的数据是否与存储在关联的校验和文件中的校验和相匹配. 如果不匹配, 则客户端可以从其他拥有块副本的 DataNode 检索该块.

### 元数据磁盘故障

FsImage 和 EditLog 是 HDFS 的核心数据结构. 这些文件的损毁可能导致 HDFS 实例失效. 因为这个原因, NameNode 能够被配置支持维护多个 FsImage 和 EditLog 副本. 任何 FsImage 或 EditLog 的更新将导致每个 FsImages 和 EditLogs 同步更新. FsImage 和 EditLog 的多副本同步更新可能降低命 NameNode 能够支持的名空间每秒事务率. 然而, 这种降低是可接受的, 因为即使 HDFS 应用程序实质上非常数据密集, 但并非原数据密集. 当 NameNode 重启时, 将选择最新的 FsImage 和 EditLog 使用.

提高故障还原能力的另一种选择是, 通过 [NFS 共享存储](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSHighAvailabilityWithNFS.html) 或使用 [分布式编辑日志](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSHighAvailabilityWithQJM.html) (称为 Journal) 使用多个 NameNodes 启用高可用性方案. 后者是推荐的方法.

### 快照

[快照](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsSnapshots.html) 支持存储特定时间点的数据副本. 快照功能的其中一个用途是可以将损毁的 HDFS 实例及时回滚到之前已知的完好的保存点.

## 数据组织

### 数据块

HDFS 为支持非常巨大的文件而设计. 兼容 HDFS 的是那些处理大型数据集的应用程序. 这些应用程序仅一次写入数据, 但一次或多次读取数据, 并且需要满足流式速度读取. HDFS 支持文件的一次写入多次读取语义. 一个典型的 HDFS 块大小是 128 MB. 因此, 一个 HDFS 文件将被切分为 128 MB 大小的分块, 如果可能话, 每个分块将分布在不同的 DataNode.

### 复制流水线

当客户端向一个复制因子为 3 的 HDFS 文件写入时, NameNode 使用一个复制目标选择算法取得一个 DataNodes 清单. 该清单包含了将托管块副本的 DataNodes. 之后客户端向第一个 DataNode 进行写入. 首个 DataNode 开始分部接收数据, 将每个部分写入本地仓库, 并将这部分传输给清单中第二个 DataNode. 第二个 DataNode, 接次开始接受每部分数据块, 写入本地仓库, 之后刷新这部分到第三个 DataNode. 最终, 第三个 DataNode 向其本地仓库写入数据. 因此, 一个 DataNode 能够从流水线中前一个 DataNode 中接收数据, 与此同时将数据发给流水线中下一个 DataNode. 因此, 数据从一个 DataNode 流向下一个.

## 可访问性

HDFS 能够通过多种不同方式被应用程序访问. 在本地, HDFS 为应用程序提供了一个 [FileSystem Java API](http://hadoop.apache.org/docs/current/api/) 供使用. [Java API 的 C 语言封装](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/LibHdfs.html) 和 [REST API](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/WebHDFS.html) 同样可用. 此外, HTTP 浏览器也能够用来浏览 HDFS 实例的文件. 通过使用 [NFS 网关](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsNfsGateway.html), HDFS 能够被挂在为本地文件系统的一部分.

### FS Shell

HDFS 允许用户数据组织成文件和目录形式. 它提供了一个称为 [FS shell](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/FileSystemShell.html) 的命令行接口, 使用户可以与 HDFS 中的数据交互. 该命令集的句法和其他用户所熟知的 shells (e.g. bash, csh) 类似. 一下是一些示例操作/命令:

| Action                          | Command                                  |
| :------------------------------ | :--------------------------------------- |
| 创建名为 `/foodir` 的目录              | `bin/hadoop dfs -mkdir /foodir`          |
| 移除名为 `/foodir` 的目录              | `bin/hadoop fs -rm -R /foodir`           |
| 查看名为 `/foodir/myfile.txt` 的文件内容 | `bin/hadoop dfs -cat /foodir/myfile.txt` |

FS shell 针对于需要通过脚本语言与存储的数据进行交互的应用程序.

### DFSAdmin

DFSAdmin 命令集用于管理 HDFS 集群. 这些事仅能被 HDFS 管理员使用的命令. 一下是一些示例操作/命令:

| Action            | Command                             |
| :---------------- | :---------------------------------- |
| 使集群进入 Safemode    | `bin/hdfs dfsadmin -safemode enter` |
| 生成 DataNodes 清单   | `bin/hdfs dfsadmin -report`         |
| 重启或关停 DataNode(s) | `bin/hdfs dfsadmin -refreshNodes`   |

### 浏览器接口

一个典型的 HDFS 会安装配置一个 web 服务器, 通过配置的 TCP 接口暴露 HDFS 命名空间. 这允许用户使用浏览器浏览 HDFS 命名空间或查看文件内容.

## 空间回收

### 文件删除和恢复

如果启用了回收站配置, 被 [FS Shell](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/FileSystemShell.html#rm) 删除的文件不会立即从 HDFS 移除. 相反, HDFS 将其移动到回收站目录下 (每个用户在 `/user/<username>/.Trash` 拥有所属的回收站). 只要保留着回收站中文件就能快速恢复.

多数最近删除的文件将被移动到当前回收站目录 (`/user/<username>/.Trash/Current`), 并且在可配置的时间间隔间, HDFS 会为当前回收站中的文件创建检查点 (`/user/<username>/.Trash/<date>`), 并删除过期的旧的检查点. 关于回收站检查点, 见 [FS shell 删除命令](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/FileSystemShell.html#expunge).

当文件在回收站中的生命到期后, NameNode 将从 HDFS 命名空间删除该文件. 删除文件将导致文件所关联的块被释放. 请注意, 用户删除文件的时间和 HDFS 中相应空间被释放的时间之间可能会有显著的延迟.

以下是一个展示了 HDFS 文件是如何通过 FS Shell 删除的例子. 我们在 delete 目录下创建了 2 个文件 (test1 & test2):

```
$ hadoop fs -mkdir -p delete/test1
$ hadoop fs -mkdir -p delete/test2
$ hadoop fs -ls delete/
Found 2 items
drwxr-xr-x   - hadoop hadoop          0 2015-05-08 12:39 delete/test1
drwxr-xr-x   - hadoop hadoop          0 2015-05-08 12:40 delete/test2
```

我们将移除 test1. 以下展示了将文件移动至 Trash 目录.

```
$ hadoop fs -rm -r delete/test1
Moved: hdfs://localhost:8020/user/hadoop/delete/test1 to trash at: hdfs://localhost:8020/user/hadoop/.Trash/Current
```

现在我们将通过 skipTrash 选项移除文件, 这不会将文件移动到 Trash. 它将完全被 HDFS 删除.

```
$ hadoop fs -rm -r -skipTrash delete/test2
Deleted delete/test2
```

我们能够看到 Trash 目录下只包含了 test1.

```
$ hadoop fs -ls .Trash/Current/user/hadoop/delete/
Found 1 items\
drwxr-xr-x   - hadoop hadoop          0 2015-05-08 12:39 .Trash/Current/user/hadoop/delete/test1
```

所以, test1 到了 Trash 目录, 而 test2 被永久删除.

### 降低复制因子

当文件的复制因子被减少, NameNode 将选择可以被删除的多余副本. 下一次 Heartbeat 将该信息传输给 DataNode. DataNode 再移除相关的块, 并且相关的空闲空间将生成在集群中. 再者, setReplication API 调用完成和空闲空间在集群中正常之间可能会由时间延迟.