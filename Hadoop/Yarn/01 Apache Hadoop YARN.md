YARN 的基本思想是将资源管理和作业调度/监控拆分至单独的守护进程. 这个想法是拥有一个全局 ResourceManager (_RM_) 和每个应用程序的 ApplicationMaster (_AM_). 一个应用程序要么是一个单独的作业, 要么是一个作业 DAG (有向无环图).

ResourceManager 和 NodeManager 构成了数据计算框架. ResourceManager 是最终权威, 它在系统中所有应用程序间进行资源仲裁. NodeManager 是每台机器负责容器的框架代理, 监视它们的资源使用 (cpu, 内存, 磁盘, 网络), 并将以上报告给 ResourceManager/Scheduler.

实际上, 每个应用的 ApplicationMaster 是框架特定的库, 其任务是从 ResourceManager 协商资源, 并与 NodeManager(s) 合作执行并监视任务.

![MapReduce NextGen Architecture](https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/yarn_architecture.gif)

ResourceManager 拥有两个主要组件: Scheduler 和 ApplicationsManager.

Scheduler 负责在容量, 队列等为人熟知的限制条件下为各个正在运行的应用程序分配资源. 在某种意义上 Scheduler 是纯调度器, 它不对应用程序的状态进行监视或跟踪. 同样, 它也不担保为由于应用程序故障或硬件故障导致失败的任务提供重启. Scheduler 基于应用程序的资源需求执行其调度函数; 它基于资源 _Container_ 的抽象概念 (包含了像内存, cpu, 磁盘, 网络等元素).

Scheduler 拥有一个可插拔的策略, 负责在各种队列, 应用程序等等之间分割集群资源. 目前的调度器像 [CapacityScheduler](https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/CapacityScheduler.html) 和 [FairScheduler](https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/FairScheduler.html) 就是一些插件示例.

ApplicationsManager 负责接收作业提交, 商定t执行应用程序特定 ApplicationMaster 的首个容器, 并提供在故障时重新启动 ApplicationMaster 容器的服务. 每个应用的 ApplicationMaster 负责与 Scheduler 商定合适的资源容器, 跟踪容器状态并监视进度.

在 hadoop-2.x, MapReduce 维护着与先前稳定版本 (hadoop-1.x) 的 **API 兼容性**. 这意味着所有 MapReduce 作业仍然能够在 YARN 上运行, 而无需更改仅需重新编译.

YARN 通过 [ReservationSystem](https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/ReservationSystem.html) 支持 **资源预留** 的概念, 这个组件允许用户指定一个有时间限制的 (over-time?) 资源配置文件和时间约束 (e.g., deadlines), 并预留资源以确保重要作业能够可预测地执行. _ReservationSystem_ 跟踪一段时间内的资源, 为预留资源执行准入控制, 并动态地指示下层调度器以确保实现预留.

为了将 YARN 扩展到数千节点以外, YARN 未来将会通过 [YARN Federation](https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/Federation.html) 支持 **Federation** 的概念. Federation 允许将多个 yarn (子) 集群透明的连接在一起, 并令它们呈现为一个的巨型集群. 这能够用于达成更大的扩展, 允许多个独立集群一起用于大型作业, 或是用于拥有它们所有容量的租户.