## 目的

本文档是用户使用 Hadoop 分布式文件系统 (HDFS) 工作的起点, 无论是是作为 Hadoop 集群的一部分还是普通的独立分布式文件系统. 然而 HDFS “仅仅是” 为了在许多环境下工作, HDFS 的工作知识对在指定集群上改进配置和诊断很有帮助.

## 概览

HDFS 是 Hadoop 应用程序主要的分布式存储系统. HDFS 集群主要由一个 NameNode (管理文件系统元数据) 和多个 DataNodes (存储实际数据). HDFS 架构指南详细的描述了 HDFS. 本指南主要处理用户和管理员与 HDFS 集群的交互. HDFS 架构图描绘了 NameNode, DataNodes, 以及客户端的基础交互. 客户端联系 NameNode 获取文件元数据或改动文件, 并通过 DataNodes 直接对实际文件进行 I/O 操作.

以下是一些可能被大量用户感兴趣的突出功能.

- Hadoop, 包括 HDFS, 很适合使用商用硬件进行分布式存储和分布式处理. 它可容错, 可拓展, 并且十分易于拓展. MapReduce, 是 Hadoop 的一部分, 以其对分布式应用的大型数据集的简单和适用性为人所知.
    
- HDFS 是高度可配置的, 其默认配置对许多安装情况的适用. 大多数情况下, 仅对于十分巨大的集群需要调整配置.
    
- Hadoop 由 Java 编写, 支持所有的主流平台.
    
- Hadoop 提供 shell-like 命令与 HDFS 直接交互.
    
- NameNode 和 Datanodes 建立了 web 服务器, 使得检查集群当前状态更容易.
    
- HDFS 定期实现新的功能和改进. 以下是一些 HDFS 有用的功能:
    
    - 文件权限和身份认证.
        
    - 机架感知: 调度任务和分配存储时考虑节点的物理位置.
        
    - Safemode: 用于维护的管理模式.
        
    - `fsck`: 用于文件系统健康诊断的工具, 寻找缺失的文件或块.
        
    - `fetchdt`: 用于获取并在本地系统文件存储 DelegationToken 的工具.
        
    - 平衡器: 当数据在 DataNodes 中分布不均匀时, 用于集群平衡的工具.
        
    - 升级和回退: 软件升级后, 能够回退到 HDFS 升级前的状态, 以防发生意料之外的问题.
        
    - Secondary NameNode: 执行命名空间的定期检查点, 并协助将 HDFS 改动日志文件的大小维持在 NameNode 当前限制内.
        
    - Checkpoint 节点: 执行命名空间定期检查点, 并协助缩小存储在 NameNode 记录 HDFS 改动的日志大小. 替代了先前 Secondary NameNode 所填补的的角色, though is not yet battle hardened. 只要系统尚未注册 Backup 节点, NameNode 就允许多个 Checkpoint 节点同时存在.
        
    - Backup 节点: Checkpoint 节点的拓展. 除了检查点, 他还接收来自 NameNode 的编辑流, 并在其内存维护命名空间的副本, 与激活的 NameNode 的命名空间状态保持同步. 一次只能为 NameNode 注册一个 Backup 节点.
        

## 前提

以下文档描述了如何安装和设置一个 Hadoop 集群:

- [单节点设置](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/SingleCluster.html) 面向新用户.
- [集群设置](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/ClusterSetup.html) 面向大型分布式集群.

以下文档假定用户能够设置并运行至少一个 DataNode 的 HDFS. 出于本文档的目的, NameNode 和 DataNode 在同一台物理设备上运行.

## Web 接口

NameNode 和 DataNode 各自运行了一个 web 服务器用于展示集群当前状态的基础信息. 在默认配置下, NameNode 主页在 `http://namenode-name:9870/`. 它列出了集群中的 DataNodes 和集群的基础统计信息. Web 接口也能用来浏览文件系统 (使用 NameNode 页面上的 “Browse the file system” 链接).

## Shell 命令

Hadoop 包含各种直接与 HDFS 以及其他 Hadoop 支持的文件系统交互的 shell 风格命令. `bin/hdfs dfs -help` 列出了 Hadoop shell 支持的命令. 此外, `bin/hdfs dfs -help command-name` 展示命令的更多帮助细节. 这些命令提供了大多数一般文件系统操作, 像复制文件, 更改文件权限等等. 也提供了部分 HDFS 独有操作, 像更改文件副本. 更多信息参见 [文件系统 Shell 指南](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/FileSystemShell.html).

### DFSAdmin 命令

`bin/hdfs dfsadmin` 命令提供了一些 HDFS 管理操作. `bin/hdfs dfsadmin -help` 命令列出来所有当前支持的命令. 例如:

- `-report`: 报告 HDFS 的基础统计信息. 其中部分信息能够在 NameNode 主页上查看.
    
- `-safemode`: 管理员能够手动进入或离开 Safemode, 尽管通常不需要.
    
- `-finalizeUpgrade`: 移除先前最后一次更新创建的集群备份.
    
- `-refreshNodes`: 通过允许连接到 namenode 的 datanodes 集合更新 namenode. 默认情况下, Namenodes 重新读取由 `dfs.hosts` 定义的 datanode 主机名, 定义在 `dfs.hosts` 的 `dfs.hosts.exclude` Hosts 的 datanodes 也是属于集群的. 如果 `dfs.hosts` 中存在条目, 只有其中的主机允许通过 namenode 注册. `dfs.hosts.exclude` 中的条目是 datanodes 需要被停止使用的. 或者, 若 `dfs.namenode.hosts.provider.classname` 被设置为 `org.apache.hadoop.hdfs.server.blockmanagement.CombinedHostFileManager`, 则所有包含和排除的主机由 `dfs.hosts` 定义的 JSON 文件中 . 当其中所有的副本都被复制到其他节点时 Datanodes 就完成了退役. 退役节点不会自动关闭, 也不回在写入新副本时被选中.
    
- `-printTopology`: 打印集群的拓扑结构. 展示 NameNode 所看到的连接着轨道的机架和 datanodes 树.
    

命令的用法参见 [dfsadmin](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html#dfsadmin).

## Secondary NameNode

NameNode 将修改存储在文件系统, 并将编辑记录作为日志追加在本机文件系统文件. 当 NameNode 开始时, 将从镜像文件 fsimage 中读取 HDFS 状态, 之后从编辑日志文件应用中应用编辑记录. 之后向 fsimage 写入新的 HDFS 状态, 并在一个空的编辑文件中开始新的操作. 由于 NameNode 仅在开始时合并 fsimage 和编辑文件, 因此在一个繁忙的集群中, 编辑日志文件可能随着时间变得十分巨大. 大型编辑文件的另一个副作用是, 下一次重启 NameNode 需要花费的时间会更长.

Secondary NameNode 定期合并 fsimage 以及编辑日志文件, 并保持编辑日志的大小在限制内. 由于它的内存需求与 Primary NameNode 等同, 因此通常在一个不同的设备上运行而不在 Primary NameNode.

Secondary NameNode 上的检查点进程的启动由两个可配置参数控制.

- `dfs.namenode.checkpoint.period`, 默认为 1 小时, 指定两个连续检查点之间的最大时间延迟
    
- `dfs.namenode.checkpoint.txns`, 默认为 1 百万, 指定 NameNode 上的非检查点事务, 及时检查点周期未达到, 将强制执行一个紧急检查点.
    

Secondary NameNode 存储着最新的检查点, 它的目录结构与 Primary NameNode 相同. 因此如果必要的话, 检查点镜像随时准备好被 Primary NameNode 读取.

关于命令用法参见 [secondarynamenode](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html#secondarynamenode).

## Checkpoint 节点

NameNode 通过两个文件维持其命名空间: fsimage (命名空间和编辑记录) 和命名空间相对检查点的改动日志. 当一个 NameNode 开启, 它将通过合并 fsimage 和编辑日志提供最新的试图文件系统元数据. 之后 NameNode 以新的 HDFS 状态覆盖 fsimage, 并开启一个新的编辑日志.

Checkpoint 节点周期性创建命名空间的检查点. 它从激活的 NameNode 上下载 fsimage 和编辑记录, 在本地合并, 并将新的镜像回传至激活的 NameNode. Checkpoint 节点通常和 NameNode 运行在不同的设备, 因为他的内存需求与 NameNode 等同. Checkpoint 节点由 `bin/hdfs namenode -checkpoint` 在配置文件指定的节点上启动.

Checkpoint (或 Backup) 节点的位置, 和它相伴的 web 接口通过 `dfs.namenode.backup.address` 和 `dfs.namenode.backup.http-address` 指定.

Checkpoint 节点上检查点进程的开启由两个可配置参数控制.

- `dfs.namenode.checkpoint.period`, 默认为 1 小时, 指定两个连续检查点之间的最大时间延迟
    
- `dfs.namenode.checkpoint.txns`, 默认为 1 百万, 指定 NameNode 上的非检查点事务, 及时检查点周期未达到, 将强制执行一个紧急检查点.
    

Checkpoint 节点存储着与 NameNode 相同的目录结构的最新检查点. 因此如果必要的话, 检查点镜像随时准备好被 NameNode 读取. 参见导入检查点.

集群配置文件中可以指定多个 chjeckpoint 节点.

关于命令的用法参见 [namenode](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html#namenode).

## Backup 节点

Backup 节点提供与 Checkpoint 节点相同的检查点功能, 以及在内存中维护着一个与在用 NameNode 保持状态同步的最新文件系统命名空间副本. 在接收来自 NameNode 文件系统编辑记录日志流并保存到硬盘期间, Backup 节点也将这些编辑记录应用到自己内存中的命名空间副本上, 从而创建了一个命名空间副本.

Backup 节点并不需要通过从在用 NameNode 下载 fsimage 和编辑记录文件创建检查点, 像 Checkpoint 节点或 Secondary NameNode 所需的那样, 因为它已经在内存中拥有了命名空间状态的最新状态. Backup 节点的检查点进程更加高效, 是因为它只需要将命名空间保存至本地 fsimage 文件并重置编辑记录.

由于 Backup 节点在内存中维护着一份命名空间副本, 它的 RAM 需求与 NameNode 相同.

NameNode 同一时刻只支持一个 Backup 节点. 当 Backup 节点在用时将不可以注册 Checkpoint 节点. 未来将会支持同时使用多个 Backup 节点.

Backup 节点与 Checkpoint 节点的配置方法相同. 通过 `bin/hdfs namenode -backup` 开启.

Backup (或是 Checkpoint) 节点的位置以及相伴的网络接口通过 `dfs.namenode.backup.address` 和 `dfs.namenode.backup.http-address` 配置.

使用 Backup 节点提供了在非持久化存储中运行 NameNode 的选项, 命名空间状态持久化职责全部委派给 Backup 节点. 为此, 通过 `-importCheckpoint` 选项启动 NameNode, 在 NameNode 配置中为编辑记录指定非持久化存储目录 `dfs.namenode.edits.dir` .

关于创建 Backup 节点和 Checkpoint 节点动机的完整的讨论参见 [HADOOP-4539](https://issues.apache.org/jira/browse/HADOOP-4539). 更多命令用法参见 [namenode](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html#namenode).

## 导入检查点

当所有的其他镜像文件和编辑记录的副本遗失时, 可以将最新的检查点导入 NameNode. 做到这一点需要:

- 创建一个指定在 `dfs.namenode.name.dir` 的空目录;
    
- 在 `dfs.namenode.checkpoint.dir`, 指定检查点目录位置;
    
- 通过 `-importCheckpoint` 选项启动 NameNode.
    

NameNode 将从 `dfs.namenode.checkpoint.dir` 目录下上传检查点, 之后将其保存到设置在 `dfs.namenode.name.dir` 的 NameNode 目录下. 如果  `dfs.namenode.name.dir` 目录下包含一个合法的镜像, NameNode 将会失败. NameNode 会验证 `dfs.namenode.checkpoint.dir` 目录下的镜像是否一致, 但不会修改它.

关于命令的用法参见 [namenode](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html#namenode).

## 平衡器

HDFS 数据在 DataNode 上可能并不总是均匀分布的. 一个常见的缘由是向现有集群添加新的 DataNodes. 当排布新的块 (一份文件内的数据将存储为一系列块), NameNode 在选择接收这些块的 DataNodes 时会考虑各种参数. 其中部分考虑因素有:

- Policy to keep one of the replicas of a block on the same node as the node that is writing the block.
    
- Need to spread different replicas of a block across the racks so that cluster can survive loss of whole rack.
    
- One of the replicas is usually placed on the same rack as the node writing to the file so that cross-rack network I/O is reduced.
    
- Spread HDFS data uniformly across the DataNodes in the cluster.
    

由于多个考虑因素存在竞争, 数据通常不会均匀的跨 DataNodes 分布. HDFS 为管理员提供了一款分析块放置并跨 DataNode 重新平衡数据的工具. 关于平衡器的管理员指南可以在 [HADOOP-1652](https://issues.apache.org/jira/browse/HADOOP-1652) 找到.

平衡器支持两个模式: 作为一款工具运行, 或是长期运行的服务:

- 在工具模式下, 它将尝试尽力平衡集群, 在达成以下条件时退出:
    
    - 所有集群一平衡.
        
    - 多次迭代中没有任何字节被移动 (默认为 5).
        
    - 没有能够移动的块.
        
    - 集群正在更新.
        
    - 其他错误.
        
- 在服务模式下, 它将作为一个长期运行的守护进程服务运行. It works like this:
    
    - 每轮次它将试图平衡集群, 直到成功或返回一个错误.
        
    - 你能够设置轮次间的时间间隔, 通过 `dfs.balancer.service.interval` 设置.
        
    - 当遇到未预料的异常, 它将在停止服务前尝试数次, 通过 `dfs.balancer.service.retries.on.exception` 设置.
        

关于命令的用法参见 [balancer](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html#balancer).

## 机架感知

HDFS 集群能够识别机架上每个节点分布的拓扑结构. 配置拓扑结构对优化数据体积和使用很重要. 更多细节请查看 [机架感知](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/RackAwareness.html) 文档.

## Safemode

During start up the NameNode loads the file system state from the fsimage and the edits log file. It then waits for DataNodes to report their blocks so that it does not prematurely start replicating the blocks though enough replicas already exist in the cluster. During this time NameNode stays in Safemode. Safemode for the NameNode is essentially a read-only mode for the HDFS cluster, where it does not allow any modifications to file system or blocks. Normally the NameNode leaves Safemode automatically after the DataNodes have reported that most file system blocks are available. If required, HDFS could be placed in Safemode explicitly using `bin/hdfs dfsadmin -safemode` command. NameNode front page shows whether Safemode is on or off. A more detailed description and configuration is maintained as JavaDoc for `setSafeMode()`.

## fsck

HDFS supports the fsck command to check for various inconsistencies. It is designed for reporting problems with various files, for example, missing blocks for a file or under-replicated blocks. Unlike a traditional fsck utility for native file systems, this command does not correct the errors it detects. Normally NameNode automatically corrects most of the recoverable failures. By default fsck ignores open files but provides an option to select all files during reporting. The HDFS fsck command is not a Hadoop shell command. It can be run as `bin/hdfs fsck`. For command usage, see [fsck](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html#fsck). fsck can be run on the whole file system or on a subset of files.

## fetchdt

HDFS supports the fetchdt command to fetch Delegation Token and store it in a file on the local system. This token can be later used to access secure server (NameNode for example) from a non secure client. Utility uses either RPC or HTTPS (over Kerberos) to get the token, and thus requires kerberos tickets to be present before the run (run kinit to get the tickets). The HDFS fetchdt command is not a Hadoop shell command. It can be run as `bin/hdfs fetchdt DTfile`. After you got the token you can run an HDFS command without having Kerberos tickets, by pointing `HADOOP_TOKEN_FILE_LOCATION` environmental variable to the delegation token file. For command usage, see [fetchdt](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html#fetchdt) command.

## Recovery Mode

Typically, you will configure multiple metadata storage locations. Then, if one storage location is corrupt, you can read the metadata from one of the other storage locations.

However, what can you do if the only storage locations available are corrupt? In this case, there is a special NameNode startup mode called Recovery mode that may allow you to recover most of your data.

You can start the NameNode in recovery mode like so: `namenode -recover`

When in recovery mode, the NameNode will interactively prompt you at the command line about possible courses of action you can take to recover your data.

If you don’t want to be prompted, you can give the `-force` option. This option will force recovery mode to always select the first choice. Normally, this will be the most reasonable choice.

Because Recovery mode can cause you to lose data, you should always back up your edit log and fsimage before using it.

## Upgrade and Rollback

When Hadoop is upgraded on an existing cluster, as with any software upgrade, it is possible there are new bugs or incompatible changes that affect existing applications and were not discovered earlier. In any non-trivial HDFS installation, it is not an option to loose any data, let alone to restart HDFS from scratch. HDFS allows administrators to go back to earlier version of Hadoop and rollback the cluster to the state it was in before the upgrade. HDFS upgrade is described in more detail in [Hadoop Upgrade](http://wiki.apache.org/hadoop/Hadoop_Upgrade) Wiki page. HDFS can have one such backup at a time. Before upgrading, administrators need to remove existing backup using bin/hadoop dfsadmin `-finalizeUpgrade` command. The following briefly describes the typical upgrade procedure:

- Before upgrading Hadoop software, finalize if there an existing backup.
    
- Stop the cluster and distribute new version of Hadoop.
    
- Run the new version with `-upgrade` option (`sbin/start-dfs.sh -upgrade`).
    
- Most of the time, cluster works just fine. Once the new HDFS is considered working well (may be after a few days of operation), finalize the upgrade. Note that until the cluster is finalized, deleting the files that existed before the upgrade does not free up real disk space on the DataNodes.
    
- If there is a need to move back to the old version,
    
    - stop the cluster and distribute earlier version of Hadoop.
        
    - run the rollback command on the namenode (`bin/hdfs namenode -rollback`).
        
    - start the cluster with rollback option. (`sbin/start-dfs.sh -rollback`).
        

When upgrading to a new version of HDFS, it is necessary to rename or delete any paths that are reserved in the new version of HDFS. If the NameNode encounters a reserved path during upgrade, it will print an error like the following:

`/.reserved is a reserved path and .snapshot is a reserved path component in this version of HDFS. Please rollback and delete or rename this path, or upgrade with the -renameReserved [key-value pairs] option to automatically rename these paths during upgrade.`

Specifying `-upgrade -renameReserved [optional key-value pairs]` causes the NameNode to automatically rename any reserved paths found during startup. For example, to rename all paths named `.snapshot` to `.my-snapshot` and `.reserved` to `.my-reserved`, a user would specify `-upgrade -renameReserved .snapshot=.my-snapshot,.reserved=.my-reserved`.

If no key-value pairs are specified with `-renameReserved`, the NameNode will then suffix reserved paths with `.<LAYOUT-VERSION>.UPGRADE_RENAMED`, e.g. `.snapshot.-51.UPGRADE_RENAMED`.

There are some caveats to this renaming process. It’s recommended, if possible, to first `hdfs dfsadmin -saveNamespace` before upgrading. This is because data inconsistency can result if an edit log operation refers to the destination of an automatically renamed file.

## DataNode Hot Swap Drive

Datanode supports hot swappable drives. The user can add or replace HDFS data volumes without shutting down the DataNode. The following briefly describes the typical hot swapping drive procedure:

- If there are new storage directories, the user should format them and mount them appropriately.
    
- The user updates the DataNode configuration `dfs.datanode.data.dir` to reflect the data volume directories that will be actively in use.
    
- The user runs `dfsadmin -reconfig datanode HOST:PORT start` to start the reconfiguration process. The user can use `dfsadmin -reconfig datanode HOST:PORT status` to query the running status of the reconfiguration task. In place of HOST:PORT, we can also specify livenodes for datanode. It would allow start or query reconfiguration on all live datanodes, whereas specifying HOST:PORT would only allow start or query of reconfiguration on the particular datanode represented by HOST:PORT. The examples for livenodes queries are `dfsadmin -reconfig datanode livenodes start` and `dfsadmin -reconfig datanode livenodes status`.
    
- Once the reconfiguration task has completed, the user can safely `umount` the removed data volume directories and physically remove the disks.
    

## File Permissions and Security

The file permissions are designed to be similar to file permissions on other familiar platforms like Linux. Currently, security is limited to simple file permissions. The user that starts NameNode is treated as the superuser for HDFS. Future versions of HDFS will support network authentication protocols like Kerberos for user authentication and encryption of data transfers. The details are discussed in the Permissions Guide.

## Scalability

Hadoop currently runs on clusters with thousands of nodes. The [PoweredBy](http://wiki.apache.org/hadoop/PoweredBy) Wiki page lists some of the organizations that deploy Hadoop on large clusters. HDFS has one NameNode for each cluster. Currently the total memory available on NameNode is the primary scalability limitation. On very large clusters, increasing average size of files stored in HDFS helps with increasing cluster size without increasing memory requirements on NameNode. The default configuration may not suite very large clusters. The [FAQ](http://wiki.apache.org/hadoop/FAQ) Wiki page lists suggested configuration improvements for large Hadoop clusters.